<h1 align="center">Welcome to pingo-server 👋</h1>

> A social network back-end infrastructure

## Install

```sh
yarn install
```

## Usage

```sh
yarn start
```

## Run tests

```sh
yarn run test:all
```

## Author

👤 **Abdallah Ahmed**

* Github: [@xUser5000](https://github.com/xUser5000)
* Facebook: [@AbdallahAhmedRabi3](https://www.facebook.com/AbdallahAhmedRabi3)

## Show your support

Give a ⭐️ if this project helped you!
